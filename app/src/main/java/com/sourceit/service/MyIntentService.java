package com.sourceit.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Student on 23.06.2018.
 */
public class MyIntentService extends IntentService {

    public MyIntentService() {
        super("IntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        int count = 0;
        for (int i = 0; i < 100_000; i++) {
            count++;
        }

        Log.d("MyIntentService", "Counter: "+count);
    }
}
